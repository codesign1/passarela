var TRANSITION_TIME = 750;

$(document).ready(function() {
    $('.hero-carousel').owlCarousel({
        singleItem : true,
        lazyLoad : true,
        autoPlay: true
    });
});

$('#menu-toggler').on('click', function() {
    var menu = $('ul.main-menu');
    if(menu.hasClass('visible')) {
        menu.removeClass('visible');
    } else {
        menu.addClass('visible');
    }
});
