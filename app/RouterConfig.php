<?php

namespace Passarela\App;

use Velocity\Core\Router;

class RouterConfig {
	public static function execute() {
		/**
		 * These two routes point to the same resource, i.e., controller when accessed
		 * using a GET request
		 */
		Router::get('/', 'us');
        Router::get('/nosotros', 'us');
        Router::get('/telas', 'telas');
        Router::get('/insumos', 'insumos');
        Router::get('/clientes', 'clientes');
        Router::get('/contactanos', 'contactanos');

		/**
		 * This is a route with basic parameters. It uses PHP regular expressions with named
		 * capture groups. The capture group name is the name of the parameter sent to the
		 * by_id method in the time controller
		*/
//		Router::get('/home/(?<id>[0-9a-zA-Z-]+)', 'home#login_try');

		/**
		 * When using multiple parameters, they are sent in order of appearance to
		 * the corresponding controller method
		 *
		 * Parameters can be defined to be of a certain type, like text or numbers only
		 */
//		Router::get('/home/(?<name>[A-z]+)/hour/(?<hour>[0-9a-zA-Z-]+)/minute/(?<minute>[0-9]+)', 'home#multiple_params');
	}
}
