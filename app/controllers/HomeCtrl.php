<?php

namespace Passarela\app\controllers;

use Velocity\Config\Config;
use Velocity\Core\Controller;
use Velocity\Helpers\Helpers;

class HomeCtrl extends Controller {

	public  $variable,
			$meta_description,
			$meta_keywords,
			$meta_autor;

	public function init() {
		$this->meta_description = 'Algo';
		$this->meta_keywords = 'Algo';
		$this->meta_autor = 'Algo';
	}

}
