<?php

namespace Passarela;

## get all the composer dependencies
include 'vendor/autoload.php';

use Velocity\Velocity;

## set up the web framework, giving it the name of the
## application
$velocity = Velocity::getInstance('Passarela');

## Definir variables globales
$velocity->add_twig_global('page_url', 'http://localhost:3000/');

## Ejecutar Velocity
$velocity->execute();
