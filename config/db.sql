-- phpMyAdmin SQL Dump
-- version 4.3.10
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Jun 23, 2016 at 09:45 PM
-- Server version: 5.5.42
-- PHP Version: 5.6.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `template`
--

-- --------------------------------------------------------

--
-- Table structure for table `v_activity`
--

CREATE TABLE `v_activity` (
  `id` int(11) NOT NULL,
  `logged` datetime NOT NULL,
  `activity` varchar(535) COLLATE utf8_unicode_ci NOT NULL,
  `category` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_from` int(11) NOT NULL,
  `user_to` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `v_album`
--

CREATE TABLE `v_album` (
  `id` int(11) NOT NULL,
  `titulo` varchar(255) NOT NULL,
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `v_balance`
--

CREATE TABLE `v_balance` (
  `id` int(11) NOT NULL,
  `user_id` varchar(535) COLLATE utf8_unicode_ci NOT NULL,
  `amount` double NOT NULL,
  `profit` double NOT NULL,
  `taxes` double NOT NULL,
  `logged` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `v_bids`
--

CREATE TABLE `v_bids` (
  `id` int(11) NOT NULL,
  `favor_id` varchar(55) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(55) COLLATE utf8_unicode_ci NOT NULL,
  `amount` double NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `v_blog`
--

CREATE TABLE `v_blog` (
  `id` int(11) NOT NULL,
  `titulo` varchar(255) NOT NULL,
  `fecha` datetime NOT NULL,
  `url` varchar(255) NOT NULL,
  `sumario` varchar(535) NOT NULL,
  `texto` text NOT NULL,
  `img` varchar(535) NOT NULL DEFAULT 'None',
  `meta_autor` varchar(535) NOT NULL DEFAULT 'Vitasnacks. Hecho por Estudio Codesign.',
  `meta_keywords` varchar(535) NOT NULL DEFAULT 'Estudio, Codesign, Vitasnacks, Noticias, Snacks Saludables',
  `meta_description` text,
  `vistas` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `v_calendario`
--

CREATE TABLE `v_calendario` (
  `id` int(11) NOT NULL,
  `logged` datetime NOT NULL,
  `user_id` int(11) NOT NULL,
  `nombre` varchar(535) NOT NULL,
  `project_id` int(11) NOT NULL,
  `day` varchar(11) NOT NULL,
  `month` varchar(11) NOT NULL,
  `year` varchar(11) NOT NULL,
  `start_time` varchar(535) NOT NULL,
  `end_time` varchar(535) NOT NULL,
  `lugar` varchar(535) NOT NULL,
  `integrantes` varchar(535) NOT NULL DEFAULT 'None',
  `desc` varchar(535) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `v_cart`
--

CREATE TABLE `v_cart` (
  `id` int(11) NOT NULL,
  `added` datetime NOT NULL,
  `user_id` int(11) NOT NULL,
  `sku` varchar(535) NOT NULL DEFAULT 'None',
  `nombre` varchar(535) NOT NULL DEFAULT 'None',
  `precio` float NOT NULL DEFAULT '0',
  `cantidad` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `v_categories`
--

CREATE TABLE `v_categories` (
  `id` int(11) NOT NULL,
  `category` varchar(535) COLLATE utf8_unicode_ci NOT NULL,
  `color` varchar(535) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `v_chats`
--

CREATE TABLE `v_chats` (
  `id` int(11) NOT NULL,
  `logged` datetime NOT NULL,
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `id_from` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `id_to` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `v_clicks`
--

CREATE TABLE `v_clicks` (
  `id` int(11) NOT NULL,
  `logged` datetime NOT NULL,
  `page` varchar(535) COLLATE utf8_unicode_ci NOT NULL,
  `ip` varchar(535) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `v_clientes`
--

CREATE TABLE `v_clientes` (
  `id` int(11) NOT NULL,
  `ano` varchar(50) NOT NULL DEFAULT '2015',
  `mes` varchar(50) NOT NULL DEFAULT '07',
  `dia` varchar(50) NOT NULL DEFAULT '16',
  `hora` varchar(50) NOT NULL DEFAULT '13:00',
  `nombre` varchar(255) NOT NULL,
  `cedula` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `fecha` varchar(255) NOT NULL,
  `creado_por` int(11) NOT NULL,
  `compras` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `v_colors`
--

CREATE TABLE `v_colors` (
  `id` int(11) NOT NULL,
  `primary` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `secondary` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(25) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `v_colors`
--

INSERT INTO `v_colors` (`id`, `primary`, `secondary`, `name`) VALUES
(1, '#3fb770', '#1a8445', 'green'),
(2, '#f2ee8a', '#e5d839', 'yellow'),
(3, '#65cced', '#179bba', 'blue'),
(4, '#ea4b6f', '#c4224c', 'pink'),
(5, '#ee974d', '#dd7222', 'orange'),
(6, '#b07ab3', '#95499b', 'purple');

-- --------------------------------------------------------

--
-- Table structure for table `v_companias`
--

CREATE TABLE `v_companias` (
  `id` int(11) NOT NULL,
  `unique_id` varchar(535) NOT NULL DEFAULT '',
  `name` varchar(535) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `v_configuracion`
--

CREATE TABLE `v_configuracion` (
  `id` int(11) NOT NULL,
  `direccion` varchar(255) NOT NULL DEFAULT 'None',
  `telefono` varchar(255) NOT NULL DEFAULT 'None',
  `celular` varchar(255) NOT NULL DEFAULT 'None',
  `email_contacto` varchar(255) NOT NULL DEFAULT 'None',
  `facebook` varchar(255) NOT NULL DEFAULT 'http://facebook.com',
  `twitter` varchar(255) NOT NULL DEFAULT 'http://twitter.com',
  `youtube` varchar(255) NOT NULL DEFAULT 'http://youtube.com',
  `instagram` varchar(535) NOT NULL DEFAULT 'http://instagram.com',
  `vimeo` varchar(255) NOT NULL DEFAULT 'http://vimeo.com',
  `linkedin` varchar(255) NOT NULL DEFAULT 'http://linkedin.com',
  `flickr` varchar(255) NOT NULL DEFAULT 'http://flickr.com',
  `googleplus` varchar(255) NOT NULL DEFAULT 'http://plus.google.com',
  `pinterest` varchar(255) NOT NULL DEFAULT 'http://pinterest.com',
  `skype` varchar(255) NOT NULL DEFAULT 'http://skype.com',
  `behance` varchar(535) NOT NULL DEFAULT 'https://www.behance.net/',
  `evernote` varchar(535) NOT NULL DEFAULT 'https://evernote.com/',
  `paypal` varchar(535) NOT NULL DEFAULT 'https://www.paypal.com/',
  `soundcloud` varchar(535) NOT NULL DEFAULT 'https://soundcloud.com/',
  `spotify` varchar(535) NOT NULL DEFAULT 'https://www.spotify.com/',
  `dribbble` varchar(535) NOT NULL DEFAULT 'https://dribbble.com/',
  `github` varchar(535) NOT NULL DEFAULT 'https://github.com/'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `v_configuracion`
--

INSERT INTO `v_configuracion` (`id`, `direccion`, `telefono`, `celular`, `email_contacto`, `facebook`, `twitter`, `youtube`, `instagram`, `vimeo`, `linkedin`, `flickr`, `googleplus`, `pinterest`, `skype`, `behance`, `evernote`, `paypal`, `soundcloud`, `spotify`, `dribbble`, `github`) VALUES
(1, 'Carrera 8 #67-74', '3136868', '3214690562', 'contacto@grupoinspiro.com', 'http://facebook.com', 'http://twitter.com', 'http://youtube.com', 'http://instagram.com', 'http://vimeo.com', 'http://linkedin.com', 'http://flickr.com', 'http://plus.google.com', 'http://pinterest.com', 'http://skype.com', 'https://www.behance.net/', 'https://evernote.com/', 'https://www.paypal.com/', 'https://soundcloud.com/', 'https://www.spotify.com/', 'https://dribbble.com/', 'https://github.com/');

-- --------------------------------------------------------

--
-- Table structure for table `v_contacto`
--

CREATE TABLE `v_contacto` (
  `id` int(11) NOT NULL,
  `logged` datetime NOT NULL,
  `name` varchar(535) NOT NULL DEFAULT 'None',
  `email` varchar(535) NOT NULL DEFAULT 'None',
  `number` varchar(535) NOT NULL DEFAULT 'None',
  `mensaje` text,
  `motivo` varchar(535) NOT NULL DEFAULT 'None'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `v_contenido`
--

CREATE TABLE `v_contenido` (
  `id` int(11) NOT NULL,
  `tabla` varchar(535) NOT NULL,
  `nombre` varchar(535) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `v_counter`
--

CREATE TABLE `v_counter` (
  `id` int(11) NOT NULL,
  `logged` datetime NOT NULL,
  `page` varchar(535) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `v_currency`
--

CREATE TABLE `v_currency` (
  `id` int(11) NOT NULL,
  `currency` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `v_equipos`
--

CREATE TABLE `v_equipos` (
  `id` int(11) NOT NULL,
  `torneo_id` varchar(11) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `color` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `v_facturas`
--

CREATE TABLE `v_facturas` (
  `id` int(11) NOT NULL,
  `numero_factura` int(1) NOT NULL,
  `ano` varchar(50) NOT NULL DEFAULT '2015',
  `mes` varchar(50) NOT NULL DEFAULT '07',
  `dia` varchar(50) NOT NULL DEFAULT '12',
  `hora` varchar(50) NOT NULL DEFAULT '12:00',
  `producto` int(11) NOT NULL,
  `tipo` varchar(50) NOT NULL DEFAULT 'sencillo',
  `precio` float NOT NULL,
  `cantidad` int(11) NOT NULL,
  `total` float NOT NULL,
  `imp` float NOT NULL,
  `total_mas_imp` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `v_facturas_master`
--

CREATE TABLE `v_facturas_master` (
  `id` int(11) NOT NULL,
  `numero` int(11) NOT NULL DEFAULT '0',
  `ano` varchar(50) NOT NULL DEFAULT '0',
  `mes` varchar(50) NOT NULL DEFAULT '0',
  `dia` varchar(50) NOT NULL DEFAULT '0',
  `hora` varchar(50) NOT NULL DEFAULT '0',
  `cliente` int(11) NOT NULL DEFAULT '0',
  `productos` int(11) NOT NULL DEFAULT '0',
  `total` float NOT NULL DEFAULT '0',
  `impuesto` float NOT NULL DEFAULT '0',
  `total_mas_impuesto` float NOT NULL DEFAULT '0',
  `hecha_por` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `v_favors`
--

CREATE TABLE `v_favors` (
  `id` int(11) NOT NULL,
  `category` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(535) COLLATE utf8_unicode_ci NOT NULL,
  `required_skills` varchar(535) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `files` text COLLATE utf8_unicode_ci NOT NULL,
  `budget` double NOT NULL,
  `featured` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'no',
  `urgent` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'no',
  `user_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bids` text COLLATE utf8_unicode_ci NOT NULL,
  `logged` datetime NOT NULL,
  `finishes` varchar(535) COLLATE utf8_unicode_ci NOT NULL,
  `candidates` text COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'incomplete',
  `hired` varchar(55) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'None'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `v_files`
--

CREATE TABLE `v_files` (
  `id` int(11) NOT NULL,
  `name` varchar(535) COLLATE utf8_unicode_ci NOT NULL,
  `file` varchar(535) COLLATE utf8_unicode_ci NOT NULL,
  `unique` varchar(535) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'None',
  `owner` varchar(535) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'None',
  `status` varchar(535) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'own'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `v_following`
--

CREATE TABLE `v_following` (
  `id` int(11) NOT NULL,
  `id_from` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `id_to` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `logged` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `v_fotos`
--

CREATE TABLE `v_fotos` (
  `id` int(11) NOT NULL,
  `nombre` varchar(535) NOT NULL,
  `uploaded` datetime NOT NULL,
  `album` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `v_friendships`
--

CREATE TABLE `v_friendships` (
  `id` int(11) NOT NULL,
  `logged` datetime NOT NULL,
  `user_to` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'request'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `v_groups`
--

CREATE TABLE `v_groups` (
  `id` int(11) NOT NULL,
  `permissions` varchar(535) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `v_imagenes`
--

CREATE TABLE `v_imagenes` (
  `id` int(11) NOT NULL,
  `torneo_id` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `equipos` varchar(535) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `v_images`
--

CREATE TABLE `v_images` (
  `id` int(11) NOT NULL,
  `user_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `img` varchar(535) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `v_info`
--

CREATE TABLE `v_info` (
  `id` int(11) NOT NULL,
  `info_name` varchar(535) COLLATE utf8_unicode_ci NOT NULL,
  `info_value` varchar(535) COLLATE utf8_unicode_ci NOT NULL,
  `category` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `price` float NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `v_invites`
--

CREATE TABLE `v_invites` (
  `id` int(11) NOT NULL,
  `email` varchar(535) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Not Accepted'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `v_invoices`
--

CREATE TABLE `v_invoices` (
  `id` int(11) NOT NULL,
  `category` varchar(535) COLLATE utf8_unicode_ci NOT NULL,
  `reference` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `buy_from` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `buyer` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `price` float NOT NULL,
  `logged` datetime NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'pending'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `v_menu`
--

CREATE TABLE `v_menu` (
  `id` int(11) NOT NULL,
  `user_id` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `menu_style` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'normal'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `v_newsletter`
--

CREATE TABLE `v_newsletter` (
  `id` int(11) NOT NULL,
  `joined` datetime NOT NULL,
  `email` varchar(535) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `edad` int(11) NOT NULL,
  `padre` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `v_notas`
--

CREATE TABLE `v_notas` (
  `id` int(11) NOT NULL,
  `torneo_id` varchar(50) NOT NULL,
  `titulo` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `sumario` varchar(535) NOT NULL,
  `fecha` datetime NOT NULL,
  `img` varchar(255) NOT NULL,
  `texto` text NOT NULL,
  `equipos` varchar(535) NOT NULL DEFAULT 'None'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `v_notifications`
--

CREATE TABLE `v_notifications` (
  `id` int(11) NOT NULL,
  `logged` datetime NOT NULL,
  `activity` varchar(535) COLLATE utf8_unicode_ci NOT NULL,
  `category` varchar(535) COLLATE utf8_unicode_ci NOT NULL,
  `user_from` int(11) NOT NULL,
  `user_to` int(11) NOT NULL DEFAULT '0',
  `flashed` varchar(11) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'no'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `v_pages`
--

CREATE TABLE `v_pages` (
  `id` int(11) NOT NULL,
  `title` varchar(535) NOT NULL,
  `slug` varchar(535) NOT NULL,
  `descripcion` text,
  `keywords` varchar(535) NOT NULL,
  `template` varchar(535) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `v_partidos`
--

CREATE TABLE `v_partidos` (
  `id` int(11) NOT NULL,
  `torneo_id` varchar(255) NOT NULL,
  `equipo_a` varchar(255) NOT NULL,
  `equipo_b` varchar(255) NOT NULL,
  `fecha` varchar(255) NOT NULL,
  `score_a` int(11) NOT NULL DEFAULT '0',
  `score_b` int(11) NOT NULL DEFAULT '0',
  `status` varchar(255) NOT NULL DEFAULT 'Sin Jugar'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `v_passwords`
--

CREATE TABLE `v_passwords` (
  `id` int(11) NOT NULL,
  `user_id` varchar(11) NOT NULL DEFAULT 'None',
  `referencia` varchar(535) NOT NULL DEFAULT 'None',
  `usuario` varchar(535) NOT NULL DEFAULT 'None',
  `password` varchar(535) NOT NULL DEFAULT 'None',
  `shared` varchar(535) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `v_pedidos`
--

CREATE TABLE `v_pedidos` (
  `id` int(11) NOT NULL,
  `unique_id` varchar(255) NOT NULL,
  `ano` varchar(50) NOT NULL,
  `mes` varchar(50) NOT NULL,
  `dia` varchar(50) NOT NULL,
  `hora` varchar(50) NOT NULL,
  `nombre` varchar(535) NOT NULL DEFAULT 'None',
  `sku` varchar(535) NOT NULL DEFAULT 'None',
  `precio` float NOT NULL DEFAULT '0',
  `cantidad` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `v_pedidos_master`
--

CREATE TABLE `v_pedidos_master` (
  `id` int(11) NOT NULL,
  `unique_id` varchar(255) NOT NULL,
  `ano` varchar(50) NOT NULL,
  `mes` varchar(50) NOT NULL,
  `dia` varchar(50) NOT NULL,
  `hora` varchar(50) NOT NULL,
  `num_productos` int(11) NOT NULL DEFAULT '0',
  `precio` float NOT NULL DEFAULT '0',
  `envio` float NOT NULL DEFAULT '0',
  `precio_mas_envio` float NOT NULL DEFAULT '0',
  `nombre` varchar(535) NOT NULL DEFAULT 'None',
  `email` varchar(535) NOT NULL DEFAULT 'None',
  `celular` varchar(535) NOT NULL DEFAULT 'None',
  `direccion` varchar(535) NOT NULL DEFAULT 'None',
  `ciudad` varchar(535) NOT NULL DEFAULT 'None',
  `suscripcion` tinyint(1) NOT NULL DEFAULT '0',
  `status` varchar(255) NOT NULL DEFAULT 'Sin Pagar',
  `entregado` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `v_posts`
--

CREATE TABLE `v_posts` (
  `id` int(11) NOT NULL,
  `user_to` varchar(535) COLLATE utf8_unicode_ci NOT NULL,
  `user_from` varchar(535) COLLATE utf8_unicode_ci NOT NULL,
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `logged` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `v_premio`
--

CREATE TABLE `v_premio` (
  `id` int(11) NOT NULL,
  `month` varchar(535) NOT NULL,
  `year` varchar(535) NOT NULL,
  `premio` varchar(535) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `v_productos`
--

CREATE TABLE `v_productos` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) NOT NULL DEFAULT 'None',
  `sku` varchar(255) NOT NULL DEFAULT 'None',
  `unidades` int(11) NOT NULL DEFAULT '1',
  `precio` float NOT NULL DEFAULT '0',
  `descripcion` text,
  `img` varchar(535) NOT NULL DEFAULT 'None',
  `tipo` varchar(535) NOT NULL DEFAULT 'None',
  `categoria` varchar(535) NOT NULL DEFAULT 'None',
  `proveedor` varchar(255) NOT NULL DEFAULT 'None',
  `medida` varchar(255) NOT NULL DEFAULT 'None',
  `peso` float NOT NULL DEFAULT '0',
  `ultimo_pedido` varchar(50) NOT NULL DEFAULT 'Nunca ha pedido',
  `inventario` int(11) NOT NULL DEFAULT '0',
  `fecha_modificado` varchar(255) DEFAULT 'Nunca',
  `persona_modificacion` int(11) NOT NULL DEFAULT '0',
  `limite_medio` int(11) NOT NULL DEFAULT '0',
  `limite_bajo` int(11) NOT NULL DEFAULT '0',
  `impuesto_nombre` varchar(255) NOT NULL DEFAULT 'IVA',
  `impuesto_valor` float NOT NULL DEFAULT '0.16'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `v_productos_complejos`
--

CREATE TABLE `v_productos_complejos` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `sku` varchar(255) NOT NULL,
  `precio` float NOT NULL DEFAULT '0',
  `img` varchar(535) NOT NULL DEFAULT 'http://vitasnacks.co/cms/uploads/products/VM-U_s15.jpg',
  `impuesto_nombre` varchar(50) NOT NULL DEFAULT 'IVA',
  `impuesto_valor` double NOT NULL DEFAULT '0.16'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `v_productos_complejos_sencillos`
--

CREATE TABLE `v_productos_complejos_sencillos` (
  `id` int(11) NOT NULL,
  `parent_sku` varchar(255) NOT NULL DEFAULT 'None',
  `producto_sku` varchar(255) NOT NULL DEFAULT 'None',
  `categoria` varchar(255) NOT NULL DEFAULT 'None',
  `medida` varchar(255) NOT NULL DEFAULT 'None',
  `cantidad` float NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `v_profileviews`
--

CREATE TABLE `v_profileviews` (
  `id` int(11) NOT NULL,
  `user_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `logged` datetime NOT NULL,
  `datee` varchar(535) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `v_projects`
--

CREATE TABLE `v_projects` (
  `id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `descripcion` varchar(535) NOT NULL,
  `creator` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `v_projects_members`
--

CREATE TABLE `v_projects_members` (
  `id` int(11) NOT NULL,
  `logged` datetime NOT NULL,
  `project_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `permissions` int(11) NOT NULL DEFAULT '3'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `v_proveedores`
--

CREATE TABLE `v_proveedores` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `telefono` varchar(255) NOT NULL,
  `direccion` varchar(255) NOT NULL,
  `contacto` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `ciudad` varchar(255) NOT NULL,
  `pedidos` int(11) NOT NULL DEFAULT '0',
  `ultimo_pedido` varchar(255) NOT NULL DEFAULT 'No Ha Hecho Pedidos'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `v_requests`
--

CREATE TABLE `v_requests` (
  `id` int(11) NOT NULL,
  `user_from` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_to` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(535) COLLATE utf8_unicode_ci NOT NULL,
  `charge` int(11) NOT NULL,
  `logged` datetime NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Pending'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `v_search`
--

CREATE TABLE `v_search` (
  `id` int(11) NOT NULL,
  `logged` datetime NOT NULL,
  `search` varchar(535) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `v_stats`
--

CREATE TABLE `v_stats` (
  `id` int(11) NOT NULL,
  `logged` datetime NOT NULL,
  `visitor` varchar(535) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Not Recorded',
  `browser_info` varchar(535) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Not Recorded',
  `language` varchar(200) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Not Recorded',
  `referer` varchar(535) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Not Recorded',
  `url` varchar(535) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Not Recorded',
  `page_title` varchar(535) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Not Recorded',
  `time_spent` varchar(535) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Not Recorded',
  `country` varchar(535) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Not Recorded',
  `region` varchar(535) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Not Recorded',
  `city` varchar(535) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Not Recorded',
  `supplier` varchar(535) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Not Recorded',
  `location` varchar(535) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Not Recorded',
  `device` varchar(535) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Not Recorded',
  `browser_name` varchar(535) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Not Recorded',
  `search_engine` varchar(535) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Not Recorded',
  `keywords` varchar(535) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Not Recorded',
  `social_network` varchar(535) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Not Recorded',
  `platform` varchar(535) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Not Recorded'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `v_sucursales`
--

CREATE TABLE `v_sucursales` (
  `id` int(11) NOT NULL,
  `nombre` varchar(535) NOT NULL,
  `direccion` varchar(535) NOT NULL,
  `telefono` varchar(535) NOT NULL,
  `contacto` varchar(535) NOT NULL,
  `email` varchar(535) NOT NULL,
  `horario` varchar(535) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `v_tabla`
--

CREATE TABLE `v_tabla` (
  `id` int(11) NOT NULL,
  `torneo_id` varchar(50) NOT NULL,
  `equipo_id` varchar(50) NOT NULL,
  `posicion` int(11) NOT NULL DEFAULT '0',
  `pts` int(11) NOT NULL DEFAULT '0',
  `pj` int(11) NOT NULL DEFAULT '0',
  `g` int(11) NOT NULL DEFAULT '0',
  `e` int(11) NOT NULL DEFAULT '0',
  `p` int(11) NOT NULL DEFAULT '0',
  `gf` int(11) NOT NULL DEFAULT '0',
  `gc` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `v_tareas`
--

CREATE TABLE `v_tareas` (
  `id` int(11) NOT NULL,
  `logged` datetime NOT NULL,
  `creator` int(11) NOT NULL,
  `tarea` varchar(535) NOT NULL,
  `desc` text NOT NULL,
  `project_id` int(11) NOT NULL,
  `prioridad` tinyint(1) NOT NULL,
  `entrega` datetime NOT NULL,
  `asignados` varchar(535) NOT NULL DEFAULT 'None',
  `completada` varchar(50) NOT NULL DEFAULT 'no',
  `fecha_completada` varchar(255) NOT NULL DEFAULT 'None'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `v_tareas_archivos`
--

CREATE TABLE `v_tareas_archivos` (
  `id` int(11) NOT NULL,
  `logged` datetime NOT NULL,
  `tarea_id` int(11) NOT NULL,
  `name` varchar(535) NOT NULL,
  `url` varchar(535) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `v_timeline`
--

CREATE TABLE `v_timeline` (
  `id` int(11) NOT NULL,
  `logged` datetime NOT NULL,
  `user_from` int(11) NOT NULL,
  `user_to` int(11) NOT NULL,
  `message` text NOT NULL,
  `img` varchar(535) NOT NULL DEFAULT 'None',
  `video` varchar(535) NOT NULL DEFAULT 'None',
  `location` varchar(535) NOT NULL DEFAULT 'None',
  `users` varchar(535) NOT NULL DEFAULT 'None'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `v_timeline_comments`
--

CREATE TABLE `v_timeline_comments` (
  `id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `user_from` int(11) NOT NULL,
  `logged` datetime NOT NULL,
  `mensaje` text NOT NULL,
  `likes` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `v_time_tracking`
--

CREATE TABLE `v_time_tracking` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `year` varchar(535) NOT NULL,
  `month` varchar(535) NOT NULL,
  `day` varchar(535) NOT NULL,
  `nombre` varchar(535) NOT NULL,
  `project_id` int(11) NOT NULL,
  `tiempo` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `v_torneos`
--

CREATE TABLE `v_torneos` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL DEFAULT 'None',
  `frase` varchar(255) NOT NULL DEFAULT 'None',
  `url` varchar(255) NOT NULL DEFAULT 'None',
  `logo` varchar(255) NOT NULL DEFAULT 'None',
  `img` varchar(255) NOT NULL DEFAULT 'None',
  `color` varchar(50) NOT NULL DEFAULT 'None',
  `logged` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `v_users`
--

CREATE TABLE `v_users` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(535) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `salt` varchar(535) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `joined` datetime NOT NULL,
  `group` int(11) NOT NULL,
  `img` varchar(535) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'uploads/userimg/default.png',
  `cover` varchar(535) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'uploads/userimg/defaultcover.jpg',
  `occupation` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Occupation...',
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Address...',
  `tel` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Tel...',
  `cel` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Cel...',
  `theme` varchar(535) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'light'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `v_users_admin`
--

CREATE TABLE `v_users_admin` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(535) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `salt` varchar(535) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `joined` datetime NOT NULL,
  `group` int(11) NOT NULL,
  `img` varchar(535) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'uploads/userimg/default.png',
  `cover` varchar(535) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'uploads/userimg/defaultcover.jpg',
  `occupation` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Occupation...',
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Address...',
  `tel` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Tel...',
  `cel` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Cel...',
  `theme` varchar(535) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'light'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `v_users_admin_session`
--

CREATE TABLE `v_users_admin_session` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `hash` varchar(535) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `v_users_session`
--

CREATE TABLE `v_users_session` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `hash` varchar(535) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `v_videos`
--

CREATE TABLE `v_videos` (
  `id` int(11) NOT NULL,
  `torneo_id` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL DEFAULT 'normal',
  `equipos` varchar(535) NOT NULL DEFAULT 'None'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `v_activity`
--
ALTER TABLE `v_activity`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_album`
--
ALTER TABLE `v_album`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_balance`
--
ALTER TABLE `v_balance`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_bids`
--
ALTER TABLE `v_bids`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_blog`
--
ALTER TABLE `v_blog`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_calendario`
--
ALTER TABLE `v_calendario`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_cart`
--
ALTER TABLE `v_cart`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_categories`
--
ALTER TABLE `v_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_chats`
--
ALTER TABLE `v_chats`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_clicks`
--
ALTER TABLE `v_clicks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_clientes`
--
ALTER TABLE `v_clientes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_colors`
--
ALTER TABLE `v_colors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_companias`
--
ALTER TABLE `v_companias`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_configuracion`
--
ALTER TABLE `v_configuracion`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_contacto`
--
ALTER TABLE `v_contacto`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_contenido`
--
ALTER TABLE `v_contenido`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_counter`
--
ALTER TABLE `v_counter`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_currency`
--
ALTER TABLE `v_currency`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_equipos`
--
ALTER TABLE `v_equipos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_facturas`
--
ALTER TABLE `v_facturas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_facturas_master`
--
ALTER TABLE `v_facturas_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_favors`
--
ALTER TABLE `v_favors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_files`
--
ALTER TABLE `v_files`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_following`
--
ALTER TABLE `v_following`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_fotos`
--
ALTER TABLE `v_fotos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_friendships`
--
ALTER TABLE `v_friendships`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_groups`
--
ALTER TABLE `v_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_imagenes`
--
ALTER TABLE `v_imagenes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_images`
--
ALTER TABLE `v_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_info`
--
ALTER TABLE `v_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_invites`
--
ALTER TABLE `v_invites`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_invoices`
--
ALTER TABLE `v_invoices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_menu`
--
ALTER TABLE `v_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_newsletter`
--
ALTER TABLE `v_newsletter`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_notas`
--
ALTER TABLE `v_notas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_notifications`
--
ALTER TABLE `v_notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_pages`
--
ALTER TABLE `v_pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_partidos`
--
ALTER TABLE `v_partidos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_passwords`
--
ALTER TABLE `v_passwords`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_pedidos`
--
ALTER TABLE `v_pedidos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_pedidos_master`
--
ALTER TABLE `v_pedidos_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_posts`
--
ALTER TABLE `v_posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_premio`
--
ALTER TABLE `v_premio`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_productos`
--
ALTER TABLE `v_productos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_productos_complejos`
--
ALTER TABLE `v_productos_complejos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_productos_complejos_sencillos`
--
ALTER TABLE `v_productos_complejos_sencillos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_profileviews`
--
ALTER TABLE `v_profileviews`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_projects`
--
ALTER TABLE `v_projects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_projects_members`
--
ALTER TABLE `v_projects_members`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_proveedores`
--
ALTER TABLE `v_proveedores`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_requests`
--
ALTER TABLE `v_requests`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_search`
--
ALTER TABLE `v_search`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_stats`
--
ALTER TABLE `v_stats`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_sucursales`
--
ALTER TABLE `v_sucursales`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_tabla`
--
ALTER TABLE `v_tabla`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_tareas`
--
ALTER TABLE `v_tareas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_tareas_archivos`
--
ALTER TABLE `v_tareas_archivos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_timeline`
--
ALTER TABLE `v_timeline`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_timeline_comments`
--
ALTER TABLE `v_timeline_comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_time_tracking`
--
ALTER TABLE `v_time_tracking`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_torneos`
--
ALTER TABLE `v_torneos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_users`
--
ALTER TABLE `v_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_users_admin`
--
ALTER TABLE `v_users_admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_users_admin_session`
--
ALTER TABLE `v_users_admin_session`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_users_session`
--
ALTER TABLE `v_users_session`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `v_videos`
--
ALTER TABLE `v_videos`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `v_activity`
--
ALTER TABLE `v_activity`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `v_album`
--
ALTER TABLE `v_album`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `v_balance`
--
ALTER TABLE `v_balance`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `v_bids`
--
ALTER TABLE `v_bids`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `v_blog`
--
ALTER TABLE `v_blog`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `v_calendario`
--
ALTER TABLE `v_calendario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `v_cart`
--
ALTER TABLE `v_cart`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `v_categories`
--
ALTER TABLE `v_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `v_chats`
--
ALTER TABLE `v_chats`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `v_clicks`
--
ALTER TABLE `v_clicks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `v_clientes`
--
ALTER TABLE `v_clientes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `v_colors`
--
ALTER TABLE `v_colors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `v_companias`
--
ALTER TABLE `v_companias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `v_configuracion`
--
ALTER TABLE `v_configuracion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `v_contacto`
--
ALTER TABLE `v_contacto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `v_contenido`
--
ALTER TABLE `v_contenido`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `v_counter`
--
ALTER TABLE `v_counter`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `v_currency`
--
ALTER TABLE `v_currency`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `v_equipos`
--
ALTER TABLE `v_equipos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `v_facturas`
--
ALTER TABLE `v_facturas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `v_facturas_master`
--
ALTER TABLE `v_facturas_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `v_favors`
--
ALTER TABLE `v_favors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `v_files`
--
ALTER TABLE `v_files`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `v_following`
--
ALTER TABLE `v_following`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `v_fotos`
--
ALTER TABLE `v_fotos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `v_friendships`
--
ALTER TABLE `v_friendships`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `v_groups`
--
ALTER TABLE `v_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `v_imagenes`
--
ALTER TABLE `v_imagenes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `v_images`
--
ALTER TABLE `v_images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `v_info`
--
ALTER TABLE `v_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `v_invites`
--
ALTER TABLE `v_invites`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `v_invoices`
--
ALTER TABLE `v_invoices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `v_menu`
--
ALTER TABLE `v_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `v_newsletter`
--
ALTER TABLE `v_newsletter`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `v_notas`
--
ALTER TABLE `v_notas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `v_notifications`
--
ALTER TABLE `v_notifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `v_pages`
--
ALTER TABLE `v_pages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `v_partidos`
--
ALTER TABLE `v_partidos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `v_passwords`
--
ALTER TABLE `v_passwords`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `v_pedidos`
--
ALTER TABLE `v_pedidos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `v_pedidos_master`
--
ALTER TABLE `v_pedidos_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `v_posts`
--
ALTER TABLE `v_posts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `v_premio`
--
ALTER TABLE `v_premio`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `v_productos`
--
ALTER TABLE `v_productos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `v_productos_complejos`
--
ALTER TABLE `v_productos_complejos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `v_productos_complejos_sencillos`
--
ALTER TABLE `v_productos_complejos_sencillos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `v_profileviews`
--
ALTER TABLE `v_profileviews`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `v_projects`
--
ALTER TABLE `v_projects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `v_projects_members`
--
ALTER TABLE `v_projects_members`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `v_proveedores`
--
ALTER TABLE `v_proveedores`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `v_requests`
--
ALTER TABLE `v_requests`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `v_search`
--
ALTER TABLE `v_search`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `v_stats`
--
ALTER TABLE `v_stats`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `v_sucursales`
--
ALTER TABLE `v_sucursales`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `v_tabla`
--
ALTER TABLE `v_tabla`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `v_tareas`
--
ALTER TABLE `v_tareas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `v_tareas_archivos`
--
ALTER TABLE `v_tareas_archivos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `v_timeline`
--
ALTER TABLE `v_timeline`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `v_timeline_comments`
--
ALTER TABLE `v_timeline_comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `v_time_tracking`
--
ALTER TABLE `v_time_tracking`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `v_torneos`
--
ALTER TABLE `v_torneos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `v_users`
--
ALTER TABLE `v_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `v_users_admin`
--
ALTER TABLE `v_users_admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `v_users_admin_session`
--
ALTER TABLE `v_users_admin_session`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `v_users_session`
--
ALTER TABLE `v_users_session`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `v_videos`
--
ALTER TABLE `v_videos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;